//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

  var bodyParser = require('body-parser');
  app.use(bodyParser.json())
  app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
    next();
  })

var requestjson = require('request-json');

var path = require('path');

var urlmovimientosMLab = "https://api.mlab.com/api/1/databases/dgerena/collections/Movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var clienteMLab = requestjson.createClient(urlmovimientosMLab);
var apiKeyMLab = 'apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt';

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
app.get('/',function(req,res){
 res.sendfile(path.join(__dirname, 'index.html'));
});

//Se Listan las cuentas y estado por cliente
app.get('/movimientos/:idcliente',function(req,res) {
  var idcliente = req.params.idcliente;
  var filtro = 'f={"cuenta": 1, "estado": 1}&';
  var queryString = 'q={"idcliente":'+ idcliente +'}&';
  clienteMLab.get('Movimientos?'+ filtro + queryString + apiKeyMLab,function(err, resM, body){
    if (err){
      console.log('Error ' + err );
      console.log(idcliente);
      console.log(body);
    }else if (body == ''){
      res.send(body);
    }
    else {
      res.send(body);
      console.log('id ' + idcliente);
      console.log('Body ' + body)
      console.log('resM ' + resM);
      console.log('queryString ' + queryString)
    }
  })
})

// Inserción de Movimientos, si la centa no existe se inserta nuevo registro
// si el cliente ya tiene registrada la cuenta se actualiza el arreglo listado (movimientos)

app.post('/updatemovimiento/:idcliente',function (req, res) {
  console.log("ingreso");
  var idcliente = req.params.idcliente;
  var cuenta = req.body.cuenta;
  var fecha = req.body.fecha;
  var importe = req.body.importe;
  var descripcion = req.body.descripcion;
  var idclienten = parseInt(idcliente);
  var cuentan = parseInt(cuenta);
  var queryString2 = 'q={"idcliente":'+ idcliente +',"cuenta":'+ cuenta +'}&';

  console.log(queryString2);

  clienteMLab.get('Movimientos?' + queryString2 + apiKeyMLab, function(err, resM, body){

    if (err){
      console.log('Error ' + err );
      console.log(idcliente);
      console.log(body);
    }else if (body.length == 0){

      var mov1 = {
          "idcliente" : idclienten ,
          "cuenta" : cuentan ,
          "estado" : "Activa" ,
           "listado" : [ { "numero" : 1 ,"fecha" : fecha , "importe" : importe , "descripcion" : descripcion}]
       }
       clienteMLab.post('Movimientos?'+ apiKeyMLab, mov1,
         function(err, resM, body) {
         console.log('Error : ' + err);
         console.log('Respuesta MLab : ' + resM);
         console.log('Body : ' + body)
         res.send(body);
      });

    }

    else {

      console.log('id ' + idcliente);
      console.log('Body ' + body)
      console.log('resM ' + resM);
      console.log('queryString ' + queryString2)


      response = body[0];
      var consulta = {};
      consulta = body[0];
      var longitud = consulta.listado.length;
      var consecutivo = longitud + 1;
      var arreglo = consulta.listado;
      var nuevomov = {
         "numero" : consecutivo ,"fecha" : fecha , "importe" : importe , "descripcion" : descripcion };
      arreglo[longitud] = nuevomov;

      console.log(body);
      console.log('Listado ' + longitud)

    var updateUser= {
       "idcliente" : consulta.idcliente,
       "cuenta" : consulta.cuenta,
       "estado" : consulta.estado,
       "listado" : arreglo
    };

      console.log('Response ' + updateUser );
      res.send(updateUser);
      clienteMLab.put('Movimientos' + '/' + response._id.$oid  + '?' + apiKeyMLab, updateUser, function(err, resM, body){
          var response = {};
          console.log(err);
          console.log(body);
          if(err) {
              response = {
                "msg" : "Error actualizando usuario."
              }
              res.status(500);
          } else {
            if(body.length > 0) {
              response = body;
            } else {
              response = {
                "msg" : "Usuario actualizado correctamente."

              }
              //res.status(404);
            }
          }
          //res.send(response);
        });
      };
      //final del else
    });
});


//Consulta movimientos por cuenta

app.get('/consultamovimiento/:idcliente/:cuenta',function (req, res) {
  var idcliente = req.params.idcliente;
  var cuenta = req.params.cuenta;
  console.log(idcliente);
  console.log(cuenta);
  let userBody = req.body;
  var filtro2 = 'f={"listado": 1}&';
  var queryString2 = 'q={"idcliente":'+ idcliente + ',"cuenta":'+ cuenta +'}&';

  console.log(queryString2);

  clienteMLab.get('Movimientos?' + filtro2 + queryString2 + apiKeyMLab, function(err, resM, body){
    console.log("entra2????");
    if (err){
      console.log('Error ' + err );
      console.log(idcliente);
      console.log(body);
    }else if (body.length == 0){
      console.log('entro');
      console.log(body);
      res.send ('bodyvacio'+ body);
    }
    else {
      res.send(body);
      console.log('id ' + idcliente);
      console.log('Body ' + body)
      console.log('resM ' + resM);
      console.log('queryString ' + queryString2)
    }
  });
});

//Gestión de login

app.post('/Usuarios/login',function(req,res) {
 var idcliente = req.body.id;
 var password = req.body.password;
 console.log(req.body.idcliente);
 console.log(req.body.password);
 var queryString = 'q={"idcliente":'+ idcliente + ' , "password": '+ '"' + password + '"' +'}&';
 clienteMLab.get('Usuarios?'+ queryString + apiKeyMLab,function(err, resM, body){
   if (err){
     console.log('Error ' + err );
     console.log(idcliente);
     console.log(body);
     res.send(body);
   }else if (body == ''){
   res.send(body);
   }
   else {
     res.send(body);
     console.log('id ' + idcliente);
     console.log('Body ' + body)
     console.log('resM ' + resM);
     console.log('queryString ' + queryString)
   }
 })
})

//Crear Usuario
app.post('/Usuarios/nuevo', function(req, res) {
  console.log("ingreso post");
  var idcliente = req.body.id;
  var nombre = req.body.nombre;
  var apellido = req.body.apellido;
  var fecha = req.body.fecha;
  var ciudad = req.body.ciudad;
  var email = req.body.email;
  var direccion = req.body.direccion;
  var password = req.body.password;
  var numero = req.body.id;
  var numero2 = parseInt(numero);
  var queryUser = 'q={"idcliente":'+ idcliente +'}&';
  var clienteMlab = requestjson.createClient(urlmovimientosMLab);
   //el numero de cliente va a ser la identificaición, se debe consultar por cliente y si existe sacar mensaje
    clienteMlab.get('Usuarios?' + queryUser + apiKeyMLab,
      function(err, resM, body) {
      console.log("busq" + queryUser);
      console.log(body);
      var nuevoUsuario = {
       "idcliente": numero2,
       "nombre": nombre,
       "apellido":apellido,
       "fecha": fecha,
       "ciudad": ciudad,
       "email": email,
       "direccion": direccion,
       "password": password
      };
      console.log("id" + nuevoUsuario);
    if (body == ''){
      clienteMlab.post('Usuarios?'+ apiKeyMLab, nuevoUsuario,
      function(err, resM, body) {
      console.log('Error : ' + err);
      console.log('Respuesta MLab : ' + resM);
      console.log('Body : ' + body);
      res.send(body);
      });
    }else {
      body = [];
      console.log("prueba " + body);
      res.send(body);
    }
 });
});
